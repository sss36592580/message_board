class CreateMessages < ActiveRecord::Migration[7.0]
  def change
    create_table :messages do |t|
      t.string :name
      t.text :message
      t.string :email
      t.integer :capacity

      t.timestamps
    end
  end
end
