Rails.application.routes.draw do

  devise_for :users

  # devise_scope :user do
  #   get '/users/sign_out' => 'devise/sessions#destroy'
  # end

  # Defines the root path route ("/")
  # root "articles#index"
  root "home#index"
  # get '/users', to: 'users#index'
  resources :home
  resources :users
end
