class HomeController < ApplicationController

    def index
        @messages = Message.page(params[:page]).per(5)
        # @messages = Message.all
    end

    def new
        @message = Message.new
    end

    def create

        @message = Message.new(message_params)
    
        # flash[:notice] = "event was successfully created"
        if @message.save
            redirect_to home_index_path, notice: 'Message was successfully created.'
        else
            render :action => :new
        end
    end

    def edit
        @message = Message.find(params[:id])
    end

    def update
        @message = Message.find(params[:id])
        @message.update(message_params)

        redirect_to home_index_path, notice: 'Message was successfully updated.'
    end

    def destroy
        @message = Message.find(params[:id])
        @message.destroy
      
        redirect_to home_index_path
    end

    private
  
    def message_params
        puts('---------')
        puts(params)
        puts('---------')
        params.require(:message).permit(:name, :message, :user_id)
        
    end
    
end


